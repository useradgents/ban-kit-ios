//
//  UABABanKit.h
//  UABABanKit
//
//  Created by Joffrey Bocquet on 22/05/15.
//  Copyright (c) 2015 userADgents. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CLBeaconRegion, CLBeacon;

//Dictionnary keys for delegate info
extern NSString *__nonnull const  UABABanKitBeaconUniqueIdkey;


//extern NSString *const UABABanKit
/**
 Information abount monitored beacon crossed.
 
 @since 0.1.0
 */
@protocol UABanMonitorDelegate <NSObject>

@optional

/**
 Let you know when the BAN SDK cross a monitored beacon.
 
 See info description key values below ..(TODO..)
 
 @param info Contain all information about the beacon crossed and the context.
 
 @since 0.1.0
 */
- (void)didEncounterBeaconWithInfos:(nonnull NSDictionary *)info;


- (void)didDedectBeacon:(nonnull CLBeacon *)beacon inRegion:(nonnull CLBeaconRegion *)region;
- (void)didEnterInRegion:(nonnull CLBeaconRegion *)region;
- (void)didExitFromRegion:(nonnull CLBeaconRegion *)region;

@end

/**
 The _`UABABanKit`_ classe is the entry point for working with **userADgents BAN Framework**. BAN stand for **B**eacon **A**d **N**etwork.
 
 You must have a _Client Token_ valid in order to use the framework.
 
 To start the SDK let UABanKit properly handle beacon encounter you just have to put the line below in your `application:didFinishLaunchingWithOptions:` method called by your application delegate. That it.

 `[[UABABanKit sharedInstanceWithToken:@"fpUHl1RuUqIq2vH6XomciV9Bn1NKj1pG9Hc1L9VX"] start];`


 Please feel free to contact us for any questions, sugestions, remarks, problems or just to say hello, at [uaban@uad.com](mailto:uaban@uad.com). You can also visit [userADgents website](http://www.useradgents.com) for more information about what we do.
 */
@interface UABABanKit : NSObject

#pragma mark -
#pragma mark Properties

/**
 Assign an object as _monitorDelegate_ and implement the mendatories methods of the protocol.
 This delegate give you callbacks about crossed beacons.
 
 @see UABanMonitorDelegate protocol reference for more information about callbacks available.
 
 @since 0.1.0
 */
@property (nonatomic, weak, nullable) id<UABanMonitorDelegate>    monitorDelegate;

#pragma mark -
#pragma mark Init

/**
 Returns a singleton instance ready to use of the _UABABanKit_ object.
 
 You must call `sharedInstanceWithToken:` to initialize the API before using
 sharedInstance.
 
 @return Instance of the shared instance previously initialized
 
 @warning Will return nil if you didn't call `sharedInstanceWithToken:` first.
 
 @since 0.1.0
 */
+ (nullable instancetype)sharedInstance;

/**
 Initializes and returns a singleton instance ready to use of the _UABABanKit_ object.
 
 You must call this method before calling any instance method of the api.
 After, when you want to make calls to _UABABanKit_ elswhere in your code, you can directly use
 `sharedInstance`.
 
 A good place to put this is in your `appDelegate` method **`application:didFinishLaunchingWithOptions:`**. :
 
 - (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
 {
 [UABABanKit sharedInstanceWithToken:@"dpU2l1Rv3x2vH6XamciviV12n1NKj1pG9Hc1L98zX"];
 
 // Override point for customization after application launch.
 return YES;
 }
 
 @param token Your client token.
 @return An initialized **UABABanKit**, ready to use instance.
 
 @since 0.1.0
 */
+ (nullable instancetype)sharedInstanceWithToken:(nonnull NSString *)token;


#pragma mark -
#pragma mark Method

/**
 Map the current device token to the backend.
 
 This method should be called each time the _deviceToken_ change. Or after each callback from the Application Delegate method: **`application:didRegisterForRemoteNotificationsWithDeviceToken:`** :
 
 - (void)application:(UIApplication *)application
 didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
 {
 [[UABABanKit sharedInstance] updateDeviceToken:deviceToken];
 }
 
 @param deviceToken A _deviceToken_ returned to you by the iOS SDK.
 
 @since 0.1.0
 */
- (void)updateDeviceToken:(nonnull NSData *)deviceToken;


/**
 Start the sdk
 You have to call this method right after the application launch.

 A good place to put this is in your `appDelegate` method **`application:didFinishLaunchingWithOptions:`**. :

 - (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
 {
 [UABABanKit sharedInstanceWithToken:@"dpU2l1Rv3x2vH6XamciviV12n1NKj1pG9Hc1L98zX"] start];

 // Override point for customization after application launch.
 return YES;
 }

 */
- (void)start;

@end
