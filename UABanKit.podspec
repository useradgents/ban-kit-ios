Pod::Spec.new do |s|
  s.name = 'UABanKit'
  s.version = '0.2.0'
  s.summary = 'The _`UABABanKit`_ classe is the entry point for working with **userADgents BAN Framework**.'
  s.license = 'MIT'
  s.authors = {"Joffrey Bocquet"=>"j.bocquet@useradgents.com"}
  s.homepage = 'https://useradgents.atlassian.net/wiki/display/BAN/BAN'
  s.description = 'The _`UABABanKit`_ classe is the entry point for working with **userADgents BAN Framework**. BAN stand for **B**eacon **A**d **N**etwork.

You must have a _Client Token_ valid in order to use the framework.

Please feel free to contact us for any questions, sugestions, remarks, problems or just to say hello, at [uaban@uad.com](mailto:uaban@uad.com). You can also visit [userADgents website](http://www.useradgents.com) for more information about what we do.'
  s.frameworks = ["CoreFoundation", "UIKit", "CoreLocation"]
  s.requires_arc = true
  s.source = { :git => "ssh://git@bitbucket.org:useradgents/ban-kit-ios.git", :tag => "v#{s.version.to_s}"}

  s.platform = :ios, '7.0'
  s.ios.platform             = :ios, '7.0'
  s.ios.preserve_paths       = 'ios/UABanKit.framework'
  s.ios.public_header_files  = 'ios/UABanKit.framework/Versions/A/Headers/*.h'
  s.ios.resource             = 'ios/UABanKit.framework/Versions/A/Resources/**/*'
  s.ios.vendored_frameworks  = 'ios/UABanKit.framework'
end
